/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{html,js}'],
  theme: {
    fontFamily: {
      sans: ['Helvetica Now Display', 'sans-serif'],
      inter: ['Inter', 'sans-serif']
    },

    container: {
      center: true,
      screens: {
        'lg': '1344px'
      }
    },

    extend: {
      colors: {
        'gray': {
          '1': '#F7F7F7',
          '2': '#C4C4C4'
        },
        'blue': {
          '1': '#5B7AE7'
        }
      }
    },
  },
  plugins: [],
}
