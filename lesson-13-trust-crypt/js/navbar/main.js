const burgerElement = document.querySelector('#burger-menu')
const navbarWrapper = document.querySelector('#navbar-wrapper')

burgerElement.addEventListener('click', ev => {
  const isMenuOpened = navbarWrapper.classList.contains('opened')

  if (isMenuOpened) {
    navbarWrapper.classList.remove('opened')
  } else {
    navbarWrapper.classList.add('opened')
  }
})
